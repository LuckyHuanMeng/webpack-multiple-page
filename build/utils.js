'use strict'
const path = require('path')
const glob = require('glob');
const config = require('../config/config')

exports.assetsPath = function (_path) {
    const assetsSubDirectory = process.env.NODE_ENV === 'production'
        ? config.build.assetsSubDirectory
        : config.dev.assetsSubDirectory

    return path.posix.join(assetsSubDirectory, _path)
}
exports.createNotifierCallback = () => {
    const notifier = require('node-notifier')

    return (severity, errors) => {
        if (severity !== 'error') return

        const error = errors[0]
        const filename = error.file && error.file.split('!').pop()

        notifier.notify({
            title: packageConfig.name,
            message: severity + ': ' + error.name,
            subtitle: filename || '',
            icon: path.join(__dirname, 'logo.png')
        })
    }
}
exports.getEntry = function () {
    var entry = {};
    //读取src目录所有page入口
    glob.sync('./src/js/**/*.js').forEach(function (name) {
        var start = name.indexOf('src/') + 4;
        var end = name.length - 3;
        var eArr = [];
        var n = name.slice(start, end);
        n = n.split('/')[1];
        eArr.push(name);
        eArr.push('babel-polyfill');
        entry[n] = eArr;
    })
    return entry;
}
exports.getHtmlConfig = function (name, chunks) {
    return {
        template: `./src/${name}.html`,
        filename: `${name}.html`,
        inject: true,
        hash: false,
        chunks: chunks
    }
}
exports.getHtmlConfigProd = function (name, chunks) {
    return {
        filename: path.resolve(__dirname, `../dist/${name}.html`),
        template: `./src/${name}.html`,
        inject: true,
        hash: false,
        minify: {
            removeComments: true,
            collapseWhitespace: true,
            removeAttributeQuotes: true
        },
        chunks: chunks
    }
}
exports.stamp = function () {
    var date = new Date();
    date = Date.parse(date);
    return date;
}
