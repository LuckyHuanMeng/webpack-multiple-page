# webpack-multiple-page

#### 介绍
使用webpack3.6.0打包多页面项目

#### 软件架构
核心：webpack3.6.0、babel-polyfill、babel相关plugins

#### 安装教程

1.  clone或下载本项目
2.  yarn install 或 npm install 安装依赖

#### 使用说明

1.  执行yarn start/npm start 启动开发服务器（已配置代理，修改ip和端口后，可进行接口调试）
2.  执行yarn run build/npm run build 可以打出生产环境的包
3.  相关配置可根据需要自行修改
4.  保持页面文件的文件名和对应的js文件名相同即可