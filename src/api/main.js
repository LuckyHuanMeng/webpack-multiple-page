import axios from 'axios'
import CryptoJS from 'crypto-js'
import Qs from 'qs'
let sha256F = function(str){
    return CryptoJS.SHA256(str).toString().toUpperCase();
};
function Encrypt(word, aesKey) {
    var key = CryptoJS.enc.Utf8.parse(aesKey);
    var encrypted = CryptoJS.AES.encrypt(word, key, {
        mode : CryptoJS.mode.ECB,
        padding : CryptoJS.pad.Pkcs7
    });
    return CryptoJS.enc.Base64.stringify(encrypted.ciphertext);
}
// const requestUrlFilterArr = [];
axios.interceptors.request.use(config=> {
    console.log(config);
    if (process.env.NODE_ENV !== 'development') {
        config.url = config.url.slice(4);
    }
    // if(!requestUrlFilterArr.includes(config.url)){
    //     var author = sessionStorage.getItem("author");
    //     var paramStr = JSON.stringify(config.data);
    //     var paramObj = {
    //         "checkKey" : sha256F(paramStr),
    //         "dataStr" : paramStr
    //     };
    //     config.data = Qs.stringify({"data": Encrypt(JSON.stringify(paramObj), author)});
    //     config.headers["Content-Type"] = 'application/x-www-form-urlencoded';
    //     config.headers["author"] = author;
    // }
    return config
})
export const mainGetList = (param) => axios.post('/temp/main/get',{});
